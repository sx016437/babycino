// TestBugI3.java
class TestBugI3 {
    public static void main(String[] args) {
        System.out.println(new Test().f());
    }
}

class Test {
    public int f() {
        int x;
        int y;
        int z;

        x = 5;
        y = 1;
        z = 0;

        // Bug: Instead of modifying x, it should remain unchanged
        z = x + y;
        x++; // Increment operation
        return z;
    }
}
