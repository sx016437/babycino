// TestBugI2.java
class TestBugI2 {
    public static void main(String[] args) {
        System.out.println(new Test().f());
    }
}

class Test {
    public int f() {
        int x;
        int y;
        int z;

        x = 5;
        y = 1;
        z = 0;

        x++; // Bug: Instead of incrementing x, it should be set to 1
        z = x + y;
        return z;
    }
}



