class TestBugI1 {
    public static void main(String[] args) {
        System.out.println(new Test().f());
    }
}

class Test {
    public int f() {
        boolean x;
        int result;

        x = true; 
        x++; // Attempt to increment a boolean variable

        if (x) {
            result = 1;
        } else {
            result = 0;
        }

        return result;
    }
}

