class TestBugI2 {
    public static void main(String[] args) {
        System.out.println(new Test().f());
    }
}

class Test {
    public int f() {
        int x;

        x = 5;
        x++; // Incorrectly increment x

        return x;
    }
}


