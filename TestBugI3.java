class TestBugI3 {
    public static void main(String[] args) {
        System.out.println(new Test().f());
    }
}

class Test {
    public int f() {
        int x;
        int y;

        x = 5;
        y = x ++;
        return y;
    }
}

